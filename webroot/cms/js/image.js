// Multiple images preview in browser
var template = $('#thumb_template').html();
var imagesPreview = function (input, placeToInsertImagePreview, id) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var images = {};
                Mustache.parse(template);
                resizeOriginal(e.target.result, 1000, 1000, images, function (original, images) {

                    images.original = original;

                    resizeThumb(original, images, function (thumb, images) {
                        images.thumb = thumb;
                        var rendered = Mustache.render(template,
                            {
                                thumb: images.thumb,
                                original: images.original
                            }
                        );
                        $(placeToInsertImagePreview).append(rendered);
                    });

                });
            }
            reader.onprogress = function (data) {
                if (data.lengthComputable) {
                    var progress = parseInt(((data.loaded / data.total) * 100), 10);
                    $("#progressBar_" + id).text(progress);
                }
            }
            reader.readAsDataURL(input.files[i]);
        }
    }

};

//Adicionar gatilhi
$(document).on("change", '.gallery-photo-add', function () {
    var uniqid = $(this).attr('data-id');
    imagesPreview(this, 'div.gallery_' + uniqid, uniqid);
});

function resizeOriginal(base64, wantedWidth, wantedHeight, images, callback) {
    //Criamos um elemento de imagem apra receber a imagem base64
    var img = document.createElement('img');
    var dataURL;

    //Quando o evento OnLoad é acionado, redimencionamos a imagem;
    img.onload = function () {
        //Criamos o Canvas e pegamos seu context
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        //Verificamos o tomanho da imagem, para redimensionarmos propocionalmente
        if (img.width > wantedWidth)
            var ratio = wantedWidth / img.width
        else if (img.height > wantedHeight)
            var ratio = wantedHeightt / img.height
        else
            var ratio = 1;

        //Seta os novos tamanhos da imagem
        canvas.width = img.width * ratio;
        canvas.height = img.height * ratio;
        //Aqui redimensionamos a imagem com o methodo canvas drawImage();
        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
        callback(canvas.toDataURL(), images);
    };
    // We put the Data URI in the image's src attribute
    img.src = base64;
}

function resizeThumb(base64, images, callback) {
    var copyImg = document.createElement('img');
    copyImg.onload = function () {
        //Copia
        var copyCanvas = document.createElement('canvas');
        var copyCtx = copyCanvas.getContext('2d');
        var MAX_WIDTH = 150;
        var MAX_HEIGHT = 150;
        copyCanvas.width = copyCanvas.height = MAX_WIDTH || MAX_WIDTH;
        copyCtx.drawImage(this, ...getWidthHeight(this, MAX_WIDTH));
        //image.thumb = copyCanvas.toDataURL();
        callback(copyCanvas.toDataURL(), images)
    }
    copyImg.src = base64;
}


// Returns a promise that resolves to the cropped
const getWidthHeight = function (img, side) {
    const {width, height} = img;
    if (width === height) {
        return [0, 0, side, side];
    } else if (width < height) {
        const rat = height / width;
        const top = (side * rat - side) / 2;
        return [0, -1 * top, side, side * rat];
    } else {
        const rat = width / height;
        const left = (side * rat - side) / 2;
        return [-1 * left, 0, side * rat, side];
    }
};




