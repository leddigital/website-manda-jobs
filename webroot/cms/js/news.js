$(document).ready(function () {
    var table = $('#posts').DataTable({
        "order": [[0, "desc"]],
        "language": {
            "lengthMenu": "_MENU_ registros por página",
            "zeroRecords": "Nenhuma informação cadastrada",
            "info": "Exibindo _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ do total de registros)",
            "sSearch": "Pesquisar",
            "paginate": {"previous": "Anterior", "next": "Próxima"}
        },
        ajax: {
            "url": "/admin/ajax/noticias",
        },
        "columnDefs": [{
            "targets": -1,
            "render": function (xhr, type, data, meta) {
                var image_name = data[3];
                return " <a href='/admin/noticias/cadastro/"+data[5]+"'>\n" +
                    "<i class='mdi mdi-18px mdi-pen'></i>\n" +
                    "</a>\n" +
                    "<a href=\"javascript:confirmDelete('/admin/noticias/cadastro/apagar/"+data[0]+"')\" class=\"on-default remove-row\">\n" +
                    "<i class=\"mdi mdi-18px mdi-delete text-danger\"></i>\n" +
                    "</a>";
            }
        }]
    });
});