
function confirmDelete(link) {
    swal({
        title: 'Confirmação!',
        text: "Deseja realmente apagar o registro?<br>Você não poderá voltar.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Apagar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result) {
            location.href = link;
        }
    });
}

$(function(){
    $('#datatable-responsive').DataTable({
        "order": [[0, "desc"]],
        "language": {
            "lengthMenu": "_MENU_ registros por página",
            "zeroRecords": "Nenhuma informação cadastrada",
            "info": "Exibindo _PAGE_ de _PAGES_",
            "infoEmpty": "Nenhum registro encontrado",
            "infoFiltered": "(Filtrado de _MAX_ do total de registros)",
            "sSearch": "Pesquisar",
            "paginate": {"previous": "Anterior", "next": "Próxima"}
        }
    });

    /*Normaliza para o nome de usuario*/
    $('.url').on("keyup", function () {
        var myStr = $(this).val()
        myStr = myStr.toLowerCase();
        //myStr=myStr.replace(/(^\s+|[^a-zA-Z0-9 ]+|\s+$)/g,"");   //this one
        myStr = myStr.replace(/\s+/g, "-");
        myStr = myStr.replace(/[\u0300-\u036f]/g, '') // Remove illegal character
        myStr = myStr.replace(/-+/g, '-')             // Remove duplicate dashes
        $(this).val(myStr);
    });

    $('input[name="title"]').on("keyup", function () {
        var string = $(this).val();
        var url = string.toString()               // Convert to string
            .normalize('NFD')               // Change diacritics
            .replace(/[\u0300-\u036f]/g,'') // Remove illegal characters
            .replace(/\s+/g,'-')            // Change whitespace to dashes
            .toLowerCase()                  // Change to lowercase
            .replace(/&/g,'-and-')          // Replace ampersand
            .replace(/[^a-z0-9\-]/g,'')     // Remove anything that is not a letter, number or dash
            .replace(/-+/g,'-')             // Remove duplicate dashes
            .replace(/^-*/,'')              // Remove starting dashes
            .replace(/-*$/,'');             // Remove trailing dashes
        $('#url').val(url);
    });

    $('input[name="name"]').on("keyup", function () {
        var string = $(this).val();
        var url = string.toString()               // Convert to string
            .normalize('NFD')               // Change diacritics
            .replace(/[\u0300-\u036f]/g,'') // Remove illegal characters
            .replace(/\s+/g,'-')            // Change whitespace to dashes
            .toLowerCase()                  // Change to lowercase
            .replace(/&/g,'-and-')          // Replace ampersand
            .replace(/[^a-z0-9\-]/g,'')     // Remove anything that is not a letter, number or dash
            .replace(/-+/g,'-')             // Remove duplicate dashes
            .replace(/^-*/,'')              // Remove starting dashes
            .replace(/-*$/,'');             // Remove trailing dashes
        $('#url').val(url);
    });


    $('input.phone-mask').attr('maxlength','15');
    $('input.phone-mask').on("keyup", function (e) {

        v = this.value;
        v = v.replace(/\D/g, "");
        this.value = v;
        if (this.value.length <= 10) {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{4})(\d)/, "$1-$2");
            this.value = v;
        } else {
            v = v.replace(/^(\d{2})(\d)/, "($1) $2");
            v = v.replace(/(\d{5})(\d)/, "$1-$2");
            this.value = v;
        }
    });


    //Funcao somente numeros
    $('input.only-number').on("keyup", function () {
        var myStr = $(this).val()
        myStr = myStr.replace(/\D/g, '')
        $(this).val(myStr);
    });
    $('input.date-mask').attr('maxlength','10');
    $('input.date-mask').on("keyup", function () {

        v = $(this).val();
        v = v.replace(/\D/g, "");
        $(this).val(v);
        v = v.replace(/^(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        $(this).val(v);
    });
});

$(document).ready(function () {
    $(".select2").select2();
    $('.summernote_complete').summernote({
        height: 400,
        toolbar: [
            ['style', ['fontsize','bold', 'italic', 'underline', 'clear']],
            ['para', ['style','ul', 'ol', 'paragraph']],
            ['insert',['picture','video','link']],
            ['misc',['codeview']]
        ],
        focus: false,
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });

    $('.summernote').summernote({
        height: 250,
        focus: false,
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['para', ['ul', 'ol', 'paragraph']],
        ],
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                document.execCommand('insertText', false, bufferText);
            }
        }
    });

    $('form').submit(function(){
        $('button[type="submit"]').html('Salvando...');
        $('button[type="submit"]').attr('disabled',true);
    });


    //Date Ranged
    moment.locale('pt-BR');

    if($('input[name="initial_date"]').val()=='' && $('input[name="final_date"]').val()=='') {
        $('input[name="initial_date"]').val(moment().format('YYYY-M-D'));
        $('input[name="final_date"]').val(moment().add(1,'month').format('YYYY-M-D'));
    }

    $('#reportrange').daterangepicker({
        format: 'MM/DD/YYYY',
        startDate: moment().subtract(29, 'days'),
        endDate: moment(),
        minDate: '01/01/2012',
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            '7 Dias': [moment(), moment().add(6, 'days')],
            '15 Dias': [moment(), moment().add(14, 'days')],
            '20 Dias': [moment(), moment().add(19, 'days')],
            '1 mês': [moment(), moment().add(1, 'month')],
            '6 meses': [moment(), moment().add(5, 'month')],
            '1 ano': [moment(), moment().add(1, 'year')],
        },
        opens: 'left',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-success',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Salvar',
            cancelLabel: 'Cancelar',
            fromLabel: 'De',
            toLabel: 'para',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1
        }
    }, function (start, end, label) {
        $('#reportrange span').html(start.format('D [de] MMMM [de] YYYY') + '     até     ' + end.format('D [de] MMMM [de] YYYY'));

            $('input[name="initial_date"]').val(start.format('YYYY-M-D'));
            $('input[name="final_date"]').val(end.format('YYYY-M-D'));

    });

    var initial_date = $('input[name=initial_date]').val();
    var final_date = $('input[name=final_date]').val();

    //Exibe a data no campo validade
    $('#reportrange span').html(moment(initial_date).format('D [de] MMMM [de] YYYY') + '    até     ' + moment(final_date).format('D [de] MMMM [de] YYYY'));
});


function loadPreview(input,id="") {

    if(id=="") {
        var img = document.getElementById('output');
    }else{
        var img = document.getElementById('output_'+id);
    }

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            // uploadCrop.croppie('bind', {
            //     url: e.target.result
            // });
            img.src = e.target.result;
        };
        reader.readAsDataURL(input.files[0]);
    } else {
        alert("Desculpe, seu navegador não suporta: FileReader API");
    }

}


