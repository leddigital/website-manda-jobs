var template = $('#thumb_template').html();
var imagesPreview = function (input, placeToInsertImagePreview) {
    if (input.files) {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var images = {};
                Mustache.parse(template);
                resizeOriginal(e.target.result, 800, 600, images, function (original, images) {

                    images.original = original;

                    resizeCrop(original, 150, 150, images, function (thumb, images) {
                        images.thumb = thumb;
                        var rendered = Mustache.render(template,
                            {
                                thumb: images.thumb,
                                original: images.original
                            }
                        );
                        $(placeToInsertImagePreview).append(rendered);
                    });

                });
            }
            reader.onloadstart = function(){
                $("#progressbar").attr('aria-valuenow',0);
                $("#progressbar").css('width',"0%");
                $("#progressbar").text("");
            }
            reader.onprogress = function (data) {
                if (data.lengthComputable) {
                    var progress = parseInt(((data.loaded / data.total) * 100), 10);
                    $("#progressbar").attr('aria-valuenow',progress);
                    $("#progressbar").css('width',progress+"%");
                    $("#progressbar").text(progress+"%");
                }
            }
            reader.result = function(){
                $("#progressbar").attr('aria-valuenow',0);
                $("#progressbar").css('width',0+"%");
                $("#progressbar").text(0+"%");
            }
            reader.readAsDataURL(input.files[i]);
        }
    }

};
//Remove o item da galeria de imagens
$(document).on("click", '#gallery_item_remove', function () {
    //Pega o ID do extra para adicionar ao input
    var id_extra = $(this).data('id');



    if (typeof id_extra != 'undefined') {
        //Template de exclusão
        var template = $('#delete_gallery_template').html();
        var rendered = Mustache.render(template,
            {
                id: id_extra,
            }
        );
        $('#gallery_item_remove_image').append(rendered);
    }

    var parent = $(this).parent();
    parent.remove();
    $("#progressbar").attr('aria-valuenow',0);
    $("#progressbar").css('width',0+"%");
    $("#progressbar").text(0+"%");
});
$(document).on("change", '.gallery-photo-add', function () {
    var uniqid = $(this).attr('data-id');
    imagesPreview(this, 'div.gallery');
});