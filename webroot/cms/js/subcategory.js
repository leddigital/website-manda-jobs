$(document).ready(function(){
 $('select[name="pages_id"]').on('change',function(){
     $.ajax({
         url: '/ajax/category',
         type:'post',
         data:'page_id='+$(this).val(),
         error: function(){
             alert('Não foi possível fazer a consulta AJAX');
         },
         beforeSend: function(){
             $("select[name='categories_id'] option").remove();
             $('select[name="categories_id"]').append($("<option />").val('carregando').text('Carregando...'));
         },
         success:function(response){
             var json = JSON.parse(response);

             $("select[name='categories_id'] option[value='carregando']").remove();

             if(json.length>0) {
                 $('select[name="categories_id"]').append($("<option />").val('').text('--- Selecione a categoria ---'));
             }else{
                 $('select[name="categories_id"]').append($("<option />").val('').text('Não há nenhuma categoria nessa página'));
             }

             $.each(json, function(i,val) {
                 $('select[name="categories_id"]').append($("<option />").val(json[i].id).text(json[i].name));
             });
         }
     });
 })
});