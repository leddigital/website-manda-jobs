$(document).ready(function () {
    'use-strict';
    $('#cep').blur(function () {
        $.ajax({
            url: '/ajax/cep/' + $(this).val(),
            error: function(){
                alert('Não foi possível fazer a consulta AJAX');
            },
            beforeSend: function(){
                $('#address').prop('disabled',true);
                $('#number').prop('disabled',true);
                $('#district').prop('disabled',true);
                $('#city').prop('disabled', true);
                $('#uf').prop('disabled', true);
            },
            success:function(response){
                var json = JSON.parse(response);
                if(json.postmon!=null) {
                    $('#address').prop('disabled', false);
                    $('#address').val(json.postmon.logradouro);
                    $('#address').prop('readonly', false);
                    $('#number').prop('disabled', false);
                    $('#district').prop('disabled', false);
                    $('#district').val(json.postmon.bairro);
                    $('#city').prop('disabled', false);
                    $('#city').val(json.postmon.cidade);
                    $('#uf').prop('disabled', false);
                    $('#uf').val(json.postmon.estado);
                    $('#number').focus();
                }else{
                    $('#address').prop('disabled',false);
                    $('#number').prop('disabled',false);
                    $('#district').prop('disabled',false);
                    $('#city').prop('disabled', false);
                    $('#uf').prop('disabled', false);
                }
            }
        });
    });
    var uploadCrop;
    function createCroppie() {
        //Croppie
        $('#upload-field').on('change', function () {
            readFile(this);
        });
        uploadCrop = $('#upload-wrapper').croppie({
            url:'/cms/img/placeholders/perfil.jpg',
            enableExif: true,
            viewport: {
                width: 250,
                height: 250,
                type: 'square'
            },
            boundary: {
                width: 250,
                height: 250
            },
            update: function () {
                uploadCrop.croppie('result', {
                    type: 'base64',
                    size: {
                        width: 500,
                        height: 500
                    }
                }).then(function (resp) {
                    $('#upload-base64').val(resp);
                });
            }
        });
    }
    $('#remove-upload').click(function(){
        $(this).addClass('hidden');
        $('#select-image').removeClass('hidden');
        if(typeof uploadCrop != "undefined") {
            uploadCrop.croppie('destroy');
        }
        $('#upload-base64').val('');
        $('input[name="id_library"]').val('');
        $('#user-image').replaceWith('<div id="upload-wrapper"></div>');
        createCroppie();

    });

    if($('#upload-wrapper').length>0) {
        createCroppie();
    }

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                uploadCrop.croppie('bind', {
                    url: e.target.result
                });
            };
            reader.readAsDataURL(input.files[0]);
        } else {
            alert("Desculpe, seu navegador não suporta: FileReader API");
        }
    }
});