<?php
setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

//Iniciar sessão
include_once '../app/paths.php';
if(file_exists(ROOT . DS . 'vendor' . DS . 'autoload.php')){
 require ROOT . DS . 'vendor' . DS . 'autoload.php';
}else{
 echo "Execute o comando 'composer install'";
 exit();
}
$settings = require APP . DS . 'settings.php';
$app = new \Slim\App($settings);
require APP . DS . 'dependencies.php';
require APP . DS . 'middleware.php';
require APP . DS . 'routes.php';
$app->run();