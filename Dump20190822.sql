-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: jsilva_mandajobs
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arquivos`
--

DROP TABLE IF EXISTS `arquivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arquivos` (
  `idarquivos` int(11) NOT NULL AUTO_INCREMENT,
  `nome_arquivo_PDF` varchar(100) NOT NULL DEFAULT 'unnamed',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `inscricao_id` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(45) NOT NULL DEFAULT 'unnamed',
  `sobrenome` varchar(45) NOT NULL DEFAULT 'unammed',
  PRIMARY KEY (`idarquivos`,`inscricao_id`),
  KEY `fk_arquivos_inscricao1_idx` (`inscricao_id`),
  CONSTRAINT `fk_arquivos_inscricao1` FOREIGN KEY (`inscricao_id`) REFERENCES `inscricao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arquivos`
--

LOCK TABLES `arquivos` WRITE;
/*!40000 ALTER TABLE `arquivos` DISABLE KEYS */;
INSERT INTO `arquivos` VALUES (1,'3636_1997.pdf','2019-08-22 14:03:50','2019-08-22 14:03:50',1,'José Silva','Costa');
/*!40000 ALTER TABLE `arquivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `candidato`
--

DROP TABLE IF EXISTS `candidato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `candidato` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `sobrenome` varchar(50) NOT NULL DEFAULT 'unsigned',
  `cpf` bigint(20) NOT NULL,
  `telefone` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `faculdade` varchar(45) NOT NULL,
  `curso` varchar(45) NOT NULL,
  `ra` varchar(45) NOT NULL,
  `id_inscricao` int(11) NOT NULL,
  `data_nascimento` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `token_validacao` varchar(100) NOT NULL,
  `isValidated` tinyint(1) NOT NULL DEFAULT '0',
  `pass` varchar(500) NOT NULL DEFAULT 'unchecked',
  `comprovante_matricula` varchar(255) DEFAULT NULL,
  `cidade` varchar(45) NOT NULL,
  `token_recuperacao` varchar(100) NOT NULL DEFAULT 'unsigned',
  `cep` varchar(45) NOT NULL DEFAULT 'unsigned',
  PRIMARY KEY (`id`,`id_inscricao`),
  UNIQUE KEY `cpf_UNIQUE` (`cpf`),
  KEY `fk_candidato_inscricao_idx` (`id_inscricao`),
  CONSTRAINT `fk_candidato_inscricao` FOREIGN KEY (`id_inscricao`) REFERENCES `inscricao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `candidato`
--

LOCK TABLES `candidato` WRITE;
/*!40000 ALTER TABLE `candidato` DISABLE KEYS */;
INSERT INTO `candidato` VALUES (1,'Edinaldo','Ribeiro',42251186816,'(17) 35224-037','edinaldoprofi99@gmail.com','UNESP','Publicidade e Propaganda','F1412035',1,'1996-02-17','2019-08-22 13:58:59','2019-08-22 14:03:41','VALIDADO',1,'$2y$10$XmeG15h341nBM./evOKzxessJ/15TlCaXqhd3rmnXbK1tWCwlh4uS','/home/edinaldo/projetos/mandajobs/webroot/projects/1/','Catanduva','unsigned','15803-255'),(2,'José Silva','Costa',27254200083,'(17) 99766-3191','lemajstor@gmail.com','Fatec Catanduva','Gestão TI','F1412035',1,'1996-02-17','2019-08-22 13:58:59','2019-08-22 14:03:41','VALIDADO',1,'$2y$10$ukZIV78LNqWRZglX78SHtulSVgG6BmOdGd0otvdbaMLk2qgu1SCeG','/home/edinaldo/projetos/mandajobs/webroot/projects/1/','Catanduva','unsigned','15803-255');
/*!40000 ALTER TABLE `candidato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inscricao`
--

DROP TABLE IF EXISTS `inscricao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inscricao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inscricao`
--

LOCK TABLES `inscricao` WRITE;
/*!40000 ALTER TABLE `inscricao` DISABLE KEYS */;
INSERT INTO `inscricao` VALUES (1,'2019-08-22 13:58:59','2019-08-22 13:58:59');
/*!40000 ALTER TABLE `inscricao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'jsilva_mandajobs'
--

--
-- Dumping routines for database 'jsilva_mandajobs'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-22 14:09:13
