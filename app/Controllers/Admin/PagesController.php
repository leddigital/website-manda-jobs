<?php

namespace App\Controllers\Admin;

use App\Models\Contents;
use App\Models\Pages;

class PagesController extends AppController
{

    public $session_id_user;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->folder = 'template/pages';
        $this->AdminView->getEnvironment()->addGlobal("_page", 'Páginas');
        $this->redirect = $this->router->pathFor('admin.pages.index');
        $this->model = new Pages();
    }

    public function dashborad($req, $res)
    {
        return $this->AdminView->render($res,'template/index.twig');
    }

    public function index($req, $res)
    {
        $collection = $this->model->get()->all();
        return $this->AdminView->render($res, $this->folder . '/index.twig', [
            'collection' => $collection
        ]);
    }

    public function form($req, $res)
    {
        //Pegar informação do banco e carregar o formulário
        $url = $req->getAttribute('url');

        if (isset($url) and $url != "") {
            $entity = $this->model->where('url', '=', $url)->get()->first();
        }
        $this->AdminView->render($res, $this->folder . '/form.twig', [
            'entity' => $entity
        ]);
    }

    public function save($req, $res)
    {
        //Formulário
        $form = $req->getParams();

        //Arquivos por upload
        $uploaded_file = $req->getUploadedFiles();
        $file1 = $uploaded_file['image1'];
        $file2 = $uploaded_file['image2'];

        //Informações da primeira imagem
        $extension1 = pathinfo($file1->getClientFilename(), PATHINFO_EXTENSION);
        $filename1 = uniqid('', true) . '.' . $extension1;
        $form['background'] = $filename1;

        //Informações da segunda imagem
        $extension2 = pathinfo($file2->getClientFilename(), PATHINFO_EXTENSION);
        $filename2 = uniqid('', true) . '.' . $extension2;
        $form['background2'] = $filename2;

        if ($form['id'] != "") {
            try {

                $entity = $this->model->find($form['id']);

                //Diretorio do upload
                $directory = WWW_ROOT . 'img' . DS . 'pages' . DS . $entity->id . DS;
                @mkdir($directory,0777,true);

                //Nome dos arquivos para o upload
                $filedirectory1 = $directory . $filename1;
                $filedirectory2 = $directory . $filename2;

                //Upload de imagem 1
                if (isset($uploaded_file['image1']) and $uploaded_file['image1']->file!="") {
                    //Excluir o arquivo anterior
                    unlink($directory.$entity->background);
                }else{
                    unset($form['background']);
                }

                //Upload de imagem 2
                if (isset($uploaded_file['image2']) and $uploaded_file['image2']->file!="") {
                    //Excluir o arquivo anterior
                    unlink($directory.$entity->background2);
                }else{
                    unset($form['background2']);
                }


                if ($entity->update($form)) {
                    //salva o arquivo
                    if (isset($uploaded_file['image1']) and $uploaded_file['image1']->file!="") {
                        if ($file1->getError() === UPLOAD_ERR_OK) {
                            $file1->moveTo($filedirectory1);
                        }
                    }
                    //salva o arquivo
                    if (isset($uploaded_file['image2']) and $uploaded_file['image2']->file!="") {
                        if ($file2->getError() === UPLOAD_ERR_OK) {
                            $file2->moveTo($filedirectory2);
                        }
                    }
                    //Adiciona logs
                    $this->logs('paginas->atualizar->' . $form['title'] . '|Cód.:' . $form['id']);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (Exception $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }
        } else {
            //Inserir novo
            try {


                //Verifica se houve arquivo para upload
                if ($uploaded_file['image1']->file=="") {
                    unset($form['background']);
                }
                if ($uploaded_file['image2']->file=="") {
                    unset($form['background2']);
                }

                print_r($form);

                //Salva um novo registro
                $created = $this->model->create($form);

                if ($created) {
                    //DIretorio do Upload
                    $directory = WWW_ROOT . 'img' . DS . 'pages' . DS . $created->id . DS;
                    @mkdir($directory,0777,true);

                    //Nome dos arquivos para o upload
                    $filedirectory1 = $directory . $filename1;
                    $filedirectory2 = $directory . $filename2;

                        //Upload da primeira imagem
                    if (isset($uploaded_file['image1']) and $uploaded_file['image1']->file!="") {
                        if ($file1->getError() === UPLOAD_ERR_OK) {
                            $file1->moveTo($filedirectory1);
                        }
                    }
                    //Upload da segunda imagem
                    if (isset($uploaded_file['image2']) and $uploaded_file['image2']->file!="") {
                        if ($file2->getError() === UPLOAD_ERR_OK) {
                            $file2->moveTo($filedirectory2);
                        }
                    }else{
                        unset($form['background2']);
                    }


                    //Criar já conteudo vinculado
                    if ($form['type'] == "content") {
                        //Criar Conteudo Vazio
                        $model_contents = new Contents();
                        $url = $this->url_verify($this->slugify($form['title']), $model_contents);
                        $content = [
                            'title' => $form['title'],
                            'url' => $url
                        ];
                        $content_created = $model_contents->create($form);
                        if ($content_created) {
                            //Salvar no banco n:m
                            $model_contents->pages()->attach($created->id, ['contents_id' => $content_created->id, 'pages_id' => $created->id]);
                        }
                    }

                    //Salvar as categorias
                    if (count($form['category']) > 0) {
                        foreach ($form['category'] as $category) {
                            $this->model->categories()->attach($created->id, ['contents_id' => $created->id, 'categories_id' => $category]);
                        }
                    }


                    //Adiciona logs
                    $this->logs('paginas->novo->' . $form['title'] . '|Cód:' . $created->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (\PDOException $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }

        }
    }

    public function delete($req, $res)
    {
        $id = $req->getAttribute('id');
        $entity = $this->model->find($id);

        try {

            //Apagar a imagem e depois remover o registro
            $directory = WWW_ROOT . 'img' . DS . 'pages' . DS . $entity->id . DS;
            unlink($directory . $entity->background);
            unlink($directory . $entity->background2);

            //Adiciona logs
            $this->logs('paginas->apagar->' . $entity->title . '|Cod:' . $entity->id);

            $entity->limitContents()->detach();
            $entity->delete();

            $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Registro removido com sucesso!']);
            return $res->withRedirect($this->redirect);
        } catch (RuntimeException $e) {
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
            return $res->withRedirect($this->redirect);
        }
    }


}
