<?php

namespace App\Controllers\Admin;

use App\Models\Categories;
use App\Models\Cities;
use App\Models\Posts;
use App\Models\PostsGallery;
use App\Models\Sponsors;

class PostsController extends AppController
{

    protected $model;
    protected $redirect;
    protected $root;

    
    public function __construct($container)
    {
        parent::__construct($container);
        $this->folder = 'template/posts';
        $this->AdminView->getEnvironment()->addGlobal("_page", 'Notícias');
        $this->AdminView->getEnvironment()->addGlobal("_page_single", 'notícia');
        $this->redirect = $this->router->pathFor('admin.posts.index');
        $this->model = new Posts();
    }
    
    public function recursivo( $caminho, $dirName ){
        
        global $dirName;
        
        $DI = new \DirectoryIterator( $caminho );
        
        foreach ($DI as $file){
            if (!$file->isDot())
            {
                if  ( $file->isFile() )
                {
                    //
                    $fileName = $file->getFilename();
                    //Caminho completo
//                    echo $caminho.DS.$fileName."<br>";
                    //Verificar se existe o nome no banco de dados.
                    $n = explode(DS, $caminho);
//                    echo $n[7];
                    $id = $n[7];
                    $post_image = Posts::where('image','=',$fileName)->get()->first();
                    if(!$post_image) {
                        $post = Posts::where('id','=',$id)->get()->first();
                        echo "Não achado: (" . $id . ")" . $fileName;
                        echo "<br>";
                        if($post){
                            //Atualizar o nome do arquivo
                            $data = ['image'=>$fileName];
                            if($post->update($data)){
                                echo "Atualizado";
                                echo "<br>";
                            }
                        }
                    }
                   
                }
            }
            
        }
    }
    
    public function image()
    {
        
        ini_set('max_execution_time', 0);
        $posts = Posts::where('created_at','>','2018-12-01')->where('created_at','<','2019-01-08')->get()->all();
        foreach($posts as $post){
            echo $post->id;
            echo "<br>";
            $data = ['updated_at'=>$post->created_at];
            $post->update($data);
        }
        
//        //Listar conteudo das pastas
//        $path = WWW_ROOT.'img'.DS.'noticias'.DS;
//        $diretorio = dir($path);
//
//       $dir = new \DirectoryIterator($path);
//        foreach($dir as $file)
//        {
//            // verifica se $file é diferente de '.' ou '..'
//            if (!$file->isDot())
//            {
//                // listando somente os diretórios
//                if  ( $file->isDir() )
//                {
//                    // atribui o nome do diretório a variável
//                    $dirName = $file->getFilename();
//
//                    // listando somente o diretório permitido
//                    //if( in_array($dirName, $diretoriosPermitidos)) {
//                        // subdiretórios
//                        $caminho = $file->getPathname();
//                        // chamada da função de recursividade
//                        $this->recursivo($caminho, $file);
//                    }
//                }
//
//                // listando somente os arquivos do diretório
//                if  ( $file->isFile() )
//                {
//                    // atribui o nome do arquivo a variável
//                    $fileName = $file->getFilename();
//                    //
//                    echo "fotos: ".$fileName."<br>";
//                }
//            }
        
    
//        $sponsors = Sponsors::all();
//        foreach ($sponsors as $post) {
//            $directory = WWW_ROOT . 'img' . DS . 'sponsors' . DS ;
//
//            $older_file = WWW_ROOT . 'img' . DS . 'sponsors' . DS . $post->filename;
//
//            $newnamefile = uniqid('', true) . '.jpg';
//
//
//            if(is_file($older_file)) {
//                $image_resize = new \App\Helpers\Image\ImageResize($older_file);
//                $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
//                //Atualizar o nome do banco de dados
//                $post->update([
//                    'filename' => $newnamefile
//                ]);
//                //Remover imagem antiga
//                unlink($older_file);
//                echo $post->id;
//                echo "<br>";
//            }else{
//                echo "Erro:".$post->id;
//                echo "<br>";
//            }
//        }
//
//        $posts = Posts::all();
//        foreach ($posts as $post) {
//            $directory = WWW_ROOT . 'img' . DS . 'noticias' . DS . $post->id . DS;
//
//            $older_file = WWW_ROOT . 'img' . DS . 'noticias' . DS . $post->id . DS . $post->image;
//
//            $newnamefile = uniqid('', true) . '.jpg';
//
//
//            if(is_file($older_file)) {
//                $image_resize = new \App\Helpers\Image\ImageResize($older_file);
//                if ($image_resize->getSourceWidth() > 800) {
//                    $image_resize->resizeToWidth(800);
//                    $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
//                } else {
//                    $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
//                }
//                //Atualizar o nome do banco de dados
//                $post->update([
//                    'image' => $newnamefile
//                ]);
//                //Remover imagem antiga
//                unlink($older_file);
//                echo $post->id;
//                echo "<br>";
//            }else{
//                echo "Erro:".$post->id;
//                echo "<br>";
//            }
//        }
    }
    
    public function paginate($req,$res){
        $params = $req->getQueryParams();
    
        $initial_date = $params['initial_date'];
        $final_date = $params['final_date'];
    
    
        $posts = Posts::join('cities_has_posts', 'posts.id', '=', 'cities_has_posts.posts_id')
            
            ->select(['posts.id', 'posts.views', 'posts.title',
                $this->db->connection()->raw('DATE_FORMAT(posts.created_at, "%d/%m/%Y %H:%i:%s") as formatted_dob'),
                $this->db->connection()->raw('(select name from cities where cities.id=cities_has_posts.cities_id) as city',function ($t) {
                    $t->on('cities.id',"=","cities_has_posts.cities_id");
                }),
                'posts.url'])
            ->get()
            ->toArray();
            
        if ($posts) {
            foreach ($posts as $items) {
                foreach ($items as $key => $item) {
                    $con[] = $item;
                }
                $j["data"][] = $con;
                unset($con);
            }
            echo json_encode($j);
        } else {
            $j["data"] = [];
            echo json_encode($j);
        }
    }
    

    public function index($req, $res)
    {

        $collection = $this->model->get()->all();
        return $this->AdminView->render($res, $this->folder . '/index.twig', [
            'collection' => $collection
        ]);
    }

    public function form($req, $res)
    {
        //Pegar informação do banco e carregar o formulário
        $url = $req->getAttribute('url');

        if (isset($url) and $url != "") {
            $entity = $this->model->where('url', '=', $url)->get()->first();
            $id = $entity->id;
            //Carrega as cidades vinculadas
            $cities = Cities::with(['posts' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('name','ASC')
                ->get();
            //Carrega as categorias vinculadas
            $categories = Categories::with(['posts' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('name','ASC')
                ->get();
            //Carregar Galeria de Imagem
            $gallery = PostsGallery::where('posts_id', $id)
                ->get();
        } else {
            //Carrega todas as categorias e cidades
            $categories = Categories::orderBy('name','ASC')
                ->get();
            $cities = Cities::orderBy('name','ASC')
                ->get();
        }

        $this->AdminView->render($res, $this->folder . '/form.twig', [
            'entity' => $entity,
            'categories' => $categories,
            'cities' => $cities,
            'gallery' => $gallery
        ]);
    }

    public function save($req, $res)
    {
        //Formulario
        $form = $req->getParams();

        //Arquivos por upload
        $uploaded_file = $req->getUploadedFiles();
        $file1 = $uploaded_file['image'];

        //Informações da primeira imagem
        $extension1 = pathinfo($file1->getClientFilename(), PATHINFO_EXTENSION);
        $filename1 = uniqid('', true) . '.' . $extension1;
        $form['image'] = $filename1;
        
        

        if ($form['id'] != "") {
    
            //Data Atualizada
            $form['updated_at'] = date('Y-m-d H:i:s');
            try {
                //Verifica se a URL já existe
                $form['url'] = $this->url_verify($form['url'], $this->model, $form['id']);

                //Editar conteudo
                $entity = $this->model->find($form['id']);


                //Atualizar a imagem principal da noticia
                $directory = WWW_ROOT . 'img' . DS . 'noticias' . DS . $entity->id . DS;
                $path_content = DS_URL . 'img' . DS_URL . 'noticias' . DS_URL . $entity->id . DS_URL;
                $filedirectory1 = $directory . $filename1;
                $directory_gallery = $directory . DS . 'gallery' . DS;
                $directory_gallery_thumbs = $directory . DS . 'gallery' . DS . 'thumbs' . DS;

                //Verifica se a imagem foi enviada, se tivesse sido enviada,
                if (isset($uploaded_file['image']) and $uploaded_file['image']->file != "") {
                    unlink($directory . $entity->image);
                } else {
                    unset($form['image']);
                }

                //Atualizar registro
                if ($entity->update($form)) {

                    //Salvar o arquivo de banner 1
                    if (isset($uploaded_file['image']) and ($uploaded_file['image']->file != "")) {
                        if ($file1->getError() === UPLOAD_ERR_OK) {
                            $file1->moveTo($filedirectory1);
                            $image_resize = new \App\Helpers\Image\ImageResize($filedirectory1);
                            $image_resize->resizeToWidth(800);
                            $newnamefile = uniqid('', true) . '.jpg';
                            $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
                            $form['image'] = $newnamefile;
                        }
                    }

                    //Remover as imagens da Galeria
                    if (count($form['delete_gallery']) > 0) {
                        foreach ($form['delete_gallery'] as $gallery_id) {
                            $gallery_entity = PostsGallery::find($gallery_id);
                            //Apaga as imagens
                            unlink($directory_gallery . $gallery_entity->photo);
                            unlink($directory_gallery_thumbs . $gallery_entity->photo);
                            $gallery_entity->delete();
                        }
                    }

                    //Atualizar as legendas da galeria de fotos
                    if (count($form['gallery_image_update_id']) > 0) {
                        foreach ($form['gallery_image_update_id'] as $key => $gallery_id) {
                            $gallery_entity = PostsGallery::find($gallery_id);
                            $infos = [
                                'legend' => $form['gallery_image_update_description'][$key]
                            ];
                            $gallery_entity->update($infos);
                        }
                    }

                    //Cadastrar novas imagens
                    if (count($form['gallery_image']) > 0 and count($form['gallery_id'] <= 0)) {
                        //Criar o diretorio, caso ele não exista
                        $directory_gallery = $directory . DS . 'gallery' . DS;
                        $directory_gallery_thumbs = $directory . DS . 'gallery' . DS . 'thumbs' . DS;
                        if (!is_dir($directory_gallery)) {
                            mkdir($directory_gallery, '777', true);
                        }
                        //Cria o diretorio de miniatura
                        if (!is_dir($directory_gallery_thumbs)) {
                            mkdir($directory_gallery_thumbs, '777', true);
                        }
                        foreach ($form['gallery_image'] as $key => $image) {
                            //Gera um novo nome de arquivo
                            preg_match('/data:([^;]*);base64,(.*)/', $image, $matches);
                            $type = explode('/', $matches[1]);
                            $extension = $type[1];
                            $filename = uniqid('gallery_') . '.' . $extension;
                            $gallery_content = [
                                'posts_id' => $entity->id,
                                'legend' => $form['gallery_image_description'][$key],
                                'photo' => $filename
                            ];
                            //Salvar as fotos no banco de dados
                            $model_posts_gallery = new PostsGallery();
                            if ($model_posts_gallery->create($gallery_content)) {
                                //Salva as fotos no diretorio
                                $this->save_photo_base64($directory_gallery, $filename, $image);
                                $this->save_photo_base64($directory_gallery_thumbs, $filename, $form['gallery_image_thumb'][$key]);
                            }
                        }
                    }


                    //Apagar relacionamento e cadastrar novamente as categorias
                    $entity->categories()->detach();
                    foreach ($form['category'] as $category) {
                        //Cadastrar
                        $entity->categories()->attach($category);
                    }


                    //Apagar relacionamento e cadastrar novamente as cidades
                    $entity->cities()->detach();
                    foreach ($form['city'] as $city) {
                        $entity->cities()->attach($city);
                    }


                }
                //Adiciona logs
                $this->logs('noticias->atualizar->' . $form['title'] . '|Cod:' . $form['id']);

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->router->pathFor('admin.posts.index'));

            } catch
            (Exception $e) {
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->router->pathFor('admin.posts.index'));
            }
        } else {
            $form['updated_at'] = date('Y-m-d H:i:s');
            //Id do usuario
            $form['users_id']= $this->_user_id;
            //Verifica se a URL já existe
            $form['url'] = $this->url_verify($form['url'], $this->model);
            //Inserir novo
            try {
                //Verifica se houve upload de arquivo, se não houver, não enviar a variavel de formulário
                if ($uploaded_file['image']->file == "") {
                    unset($form['image']);
                }

                //Salva um novo registro
                $created = $this->model->create($form);

                if ($created) {

                    $directory = WWW_ROOT . 'img' . DS . 'noticias' . DS . $created->id . DS;
                    $path_content = DS_URL . 'img' . DS_URL . 'noticias' . DS_URL . $created->id . DS_URL;
                    @mkdir($directory, 0777, true);
                    $filedirectory1 = $directory . $filename1;

                    //Upload da primeira imagem
                    if (isset($uploaded_file['image']) and $uploaded_file['image']->file != "") {
                        if ($file1->getError() === UPLOAD_ERR_OK) {
                            $file1->moveTo($filedirectory1);
                            $image_resize = new \App\Helpers\Image\ImageResize($filedirectory1);
                            $image_resize->resizeToWidth(800);
                            $newnamefile = uniqid('', true) . '.jpg';
                            $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
                            $form['image'] = $newnamefile;
                        }
                    }

                    //Salvar cidades
                    if (count($form['city']) > 0) {
                        foreach ($form['city'] as $city) {
                            $this->model->cities()->attach($created->id, ['posts_id' => $created->id, 'cities_id' => $city]);
                        }
                    }

                    //Salvar as categorias
                    if (count($form['category']) > 0) {
                        foreach ($form['category'] as $category) {
                            $this->model->categories()->attach($created->id, ['posts_id' => $created->id, 'categories_id' => $category]);
                        }
                    }

                    //Inserir nova galeria de fotos
                    if (count($form['gallery_image']) > 0 and count($form['gallery_id'] <= 0)) {
                        //Criar o diretorio, caso ele não exista
                        $directory_gallery = $directory . DS . 'gallery' . DS;
                        $directory_gallery_thumbs = $directory . DS . 'gallery' . DS . 'thumbs' . DS;
                        if (!is_dir($directory_gallery)) {
                            mkdir($directory_gallery, '777', true);
                        }
                        //Cria o diretorio de miniatura
                        if (!is_dir($directory_gallery_thumbs)) {
                            mkdir($directory_gallery_thumbs, '777', true);
                        }
                        foreach ($form['gallery_image'] as $key => $image) {
                            //Gera um novo nome de arquivo
                            preg_match('/data:([^;]*);base64,(.*)/', $image, $matches);
                            $type = explode('/', $matches[1]);
                            $extension = $type[1];
                            $filename = uniqid('gallery_') . '.' . $extension;
                            $gallery_content = [
                                'posts_id' => $created->id,
                                'legend' => $form['gallery_image_description'][$key],
                                'photo' => $filename
                            ];
                            //Salvar as fotos no banco de dados
                            $model_posts_gallery = new PostsGallery();
                            if ($model_posts_gallery->create($gallery_content)) {
                                //Salva as fotos no diretorio
                                $this->save_photo_base64($directory_gallery, $filename, $image);
                                $this->save_photo_base64($directory_gallery_thumbs, $filename, $form['gallery_image_thumb'][$key]);
                            }
                        }
                    }
                    //Adiciona logs
                    $this->logs('noticias->novo->' . $form['title'] . '|Cod:' . $created->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->router->pathFor('admin.posts.index'));
            } catch
            (\PDOException $e) {
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->router->pathFor('admin.posts.index'));
            }
        }
    }

    public function delete($req, $res)
    {
        $id = $req->getAttribute('id');

        try {

            $entity = $this->model->find($id);

            //Remove das tabelas n:m

            $entity->categories()->detach();
            $entity->cities()->detach();

            //Apagar os registros da galleria
            $gallery = PostsGallery::where('posts_id','=',$entity->id)->delete();

            //Diretorio de onde estão todos os arquivos
            $directory = WWW_ROOT . 'img' . DS . 'noticias' . DS . $entity->id . DS;

            //Apagar o conteudo
            if ($entity->delete()) {
                //Apagar todos os arquivos
                $this->rrmdir($directory);
                //Adiciona logs
                $this->logs('noticias->apagar->' . $entity->title . '|Cód:' . $entity->id);
            }

            $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Registro removido com sucesso!']);
            return $res->withRedirect($this->router->pathFor('admin.posts.index'));

        } catch (RuntimeException $e) {
            echo $e;
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
            return $res->withRedirect($this->router->pathFor('admin.posts.index'));
        }
    }

}
