<?php

namespace App\Controllers\Admin;

use App\Models\Pages;
use App\Models\Users;
use App\Models\UsersPages;
use Cake\Database\Exception;
use http\Exception\RuntimeException;
use PHPMailer\PHPMailer\PHPMailer;

class UsersController extends AppController
{

    protected $model;
    protected $redirect;
    protected $root;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->folder = 'template/users';
        $this->AdminView->getEnvironment()->addGlobal("_page", 'Usuários');
        $this->redirect = $this->router->pathFor('admin.users.index');
        $this->model = new Users();
    }

    public function index($req, $res)
    {
        $collection = $this->model->get()->all();
        $this->AdminView->render($res, $this->folder . '/index.twig', [
            'collection' => $collection
        ]);
    }

    public function form($req, $res)
    {
        //Pegar informação do banco e carregar o formulário
        $id = $req->getAttribute('id');

        //Carrega as informações salvas
        if (isset($id) and $id != "") {
            $entity = $this->model->find($id);
        }

        $this->AdminView->render($res, $this->folder . '/form.twig', [
            'entity' => $entity,
        ]);
    }

    public function save($req, $res)
    {
        //Formulario
        $form = $req->getParams();

        //Informações de imagens
        $directory = WWW_ROOT . 'img' . DS . 'perfil' . DS;
        preg_match('/data:([^;]*);base64,(.*)/', $form['upload_photo_base64'], $matches);
        $type = explode('/', $matches[1]);
        $extension = $type[1];
        $filename = uniqid('img_') . '.' . $extension;
        $form['profile'] = $filename;

        //Setar redirect
        $this->redirect = $form['redirect'] ? $form['redirect'] : $this->redirect;

        //Gera a senha criptografada
        if ($form['password'] != "") {
            $options = [
                'salt' => random_bytes(22),
            ];
            $password = password_hash($form['password'], PASSWORD_BCRYPT, $options);
            $form['password'] = $password;
        } else {
            unset($form['password']);
        }

        if ($form['id'] != "") {
            try {
                //Editar conteudo
                $entity = $this->model->find($form['id']);
                //Background 1
                if (isset($form['upload_photo_base64']) and $form['upload_photo_base64'] != "") {
                    //Apagar a imagem anterior
                    unlink($directory . $entity->profile);
                    if ($entity->update($form)) {
                        $this->save_photo_base64($directory, $filename, $form['upload_photo_base64']);
                    }
                } else {
                    unset($form['profile']);
                    //Se não houver troca de imagem, simplesmente atualiza as informações
                }

                if ($entity->update($form)) {
                    //Adiciona logs
                    $this->logs('usuarios->atualizar->' . $entity->name . '|Cod:' . $entity->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (Exception $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }
        } else {
            //Inserir novo
            try {

                $created = $this->model->create($form);
                if ($created) {
                    //Salva a nova foto
                    $this->save_photo_base64($directory, $filename, $form['upload_photo_base64']);
                    //Adiciona logs
                    $this->logs('usuarios->novo->' . $form['name'] . '|Cod:' . $created->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (\PDOException $e) {
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }

        }
    }

    public function delete($req, $res)
    {
        $id = $req->getAttribute('id');
        $entity = $this->model->find($id);

        try {

            if ($entity->delete()) {
                //Remover a imagem de perfil
                $directory = WWW_ROOT . 'img' . DS . 'perfil' . DS . $entity->profile;
                @unlink($directory);

                //Adiciona logs
                $this->logs('usuarios->apagar->' . $entity->name . '|Cod:' . $entity->id);
            }

            $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Registro removido com sucesso!']);
            return $res->withRedirect($this->redirect);
        } catch (RuntimeException $e) {
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
            return $res->withRedirect($this->redirect);
        }
    }

    public function login($req, $res)
    {
        $this->AdminView->render($res, 'template/login.twig');
    }

    public function user_login($req, $res)
    {
        $post = $req->getParams();
        $username = $post['username'];
        $password = $post['password'];
        $user = $this->model->where('email', '=', $username)->get()->first();

        $password_db = $user->password;
        $password_verify = password_verify($password, $password_db);

        if ($password_verify) {
            if ($post['remember'] == 1) {
                //Cookies
                $this->Session->setCookie('cms_user_id', $user->id);
                $this->Session->setCookie('cms_user_name', $user->name);
                $this->Session->setCookie('cms_user_permission', $user->permission);
            } else {
                //Sessions
                $this->Session->setSession('cms_user_id', $user->id);
                $this->Session->setSession('cms_user_name', $user->name);
                $this->Session->setSession('cms_user_permission', $user->permission);
            }
            //Adiciona logs
            $this->logs('Login de Usuário', $user->id);
            return $res->withRedirect($this->router->pathFor('admin.posts.index'));
        } else {
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Erro de usuário ou senha!']);
            return $res->withRedirect($this->router->pathFor('admin.login'));
        }
    }

    public function user_logout($req, $res)
    {
        //Adiciona logs
        $this->logs('Logout de Usuário');

        //Resetar Sessões
        $this->Session->sessionUnset('cms_user_id');
        $this->Session->sessionUnset('cms_user_name');
        $this->Session->sessionUnset('cms_user_root');
        //Resetar Cookies
        $this->Session->cookieUnset('cms_user_id');
        $this->Session->cookieUnset('cms_user_name');
        $this->Session->cookieUnset('cms_user_root');
        return $res->withRedirect($this->router->pathFor('admin.login'));
    }

    public function forgot_password($req, $res)
    {
        $this->AdminView->render($res, 'template/forgot_password.twig');
    }

    public function reset_password($req, $res)
    {

        $post = $req->getParams();

        $user = $this->model->where('email', '=', $post['email'])->get()->first();


        if (isset($user) and (count($user) > 0)) {

            //Gera a nova senha e a envia para o e-mail
            $senha_gerada = $this->geraSenha();
            $password = password_hash($senha_gerada, PASSWORD_BCRYPT);
            //Salvar a nova senha
            $user->update(
                ['password' => $password]
            );

            $mail = new PHPMailer();
            //Configuração
            $mail->isSMTP();
            $mail->CharSet = 'UTF-8';
            $mail->SMTPDebug = $this->mail->debug;
            $mail->Host = $this->mail->host;
            $mail->SMTPAuth = true;
            $mail->SMTPSecure = 'tls';
            $mail->Username = $this->mail->username;
            $mail->Password = $this->mail->password;
            $mail->Port = $this->mail->port;
            //Informações para Envio
            $mail->setFrom('noreply@metropolitananews.com', 'Suporte Metropolitana');
            $mail->addAddress($user->email, $user->name);
            //Corpo do e-mail
            $mail->isHTML(true);
            $mail->Subject = 'Recuperação de senha';
            $mail->msgHTML($this->AdminView->fetch('template/email/reset_password.twig', [
                'email' => $user->email,
                'senha' => $senha_gerada
            ], false));
            if ($mail->send()) {
                $this->flash->addMessage('msg', ['title' => 'Enviado!', 'type' => 'success', 'message' => 'E-mail de recuperação de senha enviado com sucesso! \n Caso não tenha recebido na Caixa de Entrada, verifique na caixa de SPAM ou LIXEIRA.']);
                return $res->withRedirect($this->router->pathFor('admin.login'));
            }else{
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu algum erro inesperado.']);
                return $res->withRedirect($this->router->pathFor('admin.users.forgot'));
            }
        } else {
            //Erro, caso o e-mail não tenha sido encontrado
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Seu e-mail não foi encontrado na nossa base de dados.']);
            return $res->withRedirect($this->router->pathFor('admin.users.forgot'));
        }
    }

}
