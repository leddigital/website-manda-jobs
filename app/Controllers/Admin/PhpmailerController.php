<?php

namespace AppHttpControllers;

use Request;

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailerPHPMailerPHPMailer;
use PHPMailerPHPMailerException;

class PhpmailerController extends Controller {

	public function sendEmail (Request $request) {
  	// is method a POST ?
  	if(Request::isMethod('post')) {
			require 'vendor/autoload.php';							            

			$mail = new PHPMailer(true);                                        

			try {
				// Server settings
				$mail->isSMTP();                                                    
				$mail->Host = 'mail.alfaagropecas.com.br';						      
				$mail->SMTPAuth = true;                                             
				$mail->Username = 'teste@alfaagropecas.com.br';                   
				$mail->Password = 'teste@alfaagro';                                
				$mail->SMTPSecure = 'tls';                                      
				$mail->Port = 587;                                                

				//Attachments (optional)
				// $mail->addAttachment('/var/tmp/file.tar.gz');			
				// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');	

				//Content
				$mail->isHTML(true); 											
				$mail->Subject = Request::input('subject');
				$mail->Body    = Request::input('message');						

                $mail->send();
                
				return back()->with('success','Message has been sent!');
			} catch (Exception $e) {
				return back()->with('error','Message could not be sent.');
			}
		}
    	return view('phpmailer');
  }
} 