<?php

namespace App\Controllers\Admin;

use App\Models\Cities;
use App\Models\SponsorLocals;
use App\Models\Sponsors;

class SponsorsController extends AppController
{

    public $session_id_user;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->folder = 'template/sponsors';
        $this->AdminView->getEnvironment()->addGlobal("_page_single", 'patrocinador');
        $this->AdminView->getEnvironment()->addGlobal("_page", 'Patrocinadores');
        $this->redirect = $this->router->pathFor('admin.sponsors.index');
        $this->model = new Sponsors();
    }

    public function index($req, $res)
    {
        $collection = $this->model->
            select('sponsors.*',
            $this->db->connection()->raw('(select count(sponsors_id) from sponsors_clicks where sponsors_clicks.sponsors_id=sponsors.id) as views',function ($t) {
                $t->on('sponsors.id',"=","sponsors_clicks.sponsors_id");
            }))
            ->get();
        return $this->AdminView->render($res, $this->folder . '/index.twig', [
            'collection' => $collection
        ]);

    }

    public function form($req, $res)
    {
        //Pegar informação do banco e carregar o formulário
        $id = $req->getAttribute('id');

        //Trazer todas as cidades
        $model_cities = new Cities();

        if (isset($id) and $id != "") {
            $entity = $this->model->find($id);
            $cities_sponsors = Cities::with(['sponsors'=>function ($q) use ($id){
                $q->where('id',$id);
            }])->get();
           
            $locals = SponsorLocals::where('sponsors_id','=',$id)->get(['local'])->pluck('local')->toArray();
        }else{
            $cities_sponsors = Cities::get()->all();
        }
        $this->AdminView->render($res, $this->folder . '/form.twig', [
            'entity' => $entity,
            'cities'=>$cities_sponsors,
            'locals'=>$locals
        ]);
    }

    public function save($req, $res)
    {
        //Formulário
        $form = $req->getParams();

        //Arquivos por upload
        $uploaded_file = $req->getUploadedFiles();
        $file = $uploaded_file['banner'];

        $extension = pathinfo($file->getClientFilename(), PATHINFO_EXTENSION);
        $filename = uniqid('pp_', true) . '.' . $extension;

        //define o diretorio do arquivo
        $directory = WWW_ROOT . 'img' . DS . 'sponsors' . DS;
        if(!is_dir($directory)){
            mkdir($directory,0777,true);
        }
        $filedirectory = $directory . $filename;
        
        $form['filename'] = $filename;

        if ($form['id'] != "") {
            try {

                $entity = $this->model->find($form['id']);

                //Arquivo de banner 1
                if (isset($uploaded_file['banner']) and ($uploaded_file['banner']->file != "")) {
                    //Excluir o arquivo anterior
                    unlink($directory . $entity->filename);
                } else {
                    unset($form['filename']);
                }
    
                //Salvar o arquivo de banner 1
                if (isset($uploaded_file['banner']) and ($uploaded_file['banner']->file != "")) {
                    if ($file->getError() === UPLOAD_ERR_OK) {
                        $file->moveTo($filedirectory);
                        $image_resize = new \App\Helpers\Image\ImageResize($filedirectory);
                        $newnamefile = uniqid('pp_', true) . '.jpg';
                        $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
                        $form['filename'] = $newnamefile;
                    }
                    unlink($filedirectory);
                }
                
                if ($entity->update($form)) {
                    
                    //Atualizar ao Local
                    $model_sponsors_local = new SponsorLocals();
                    //Apagar todos os locais de propaganda
                    $model_sponsors_local->where('sponsors_id','=',$form['id'])->delete();
                    foreach ($form['local'] as $local) {
                        $model_sponsors_local->create(
                            [
                                'sponsors_id' => $form['id'],
                                'local' => $local
                            ]
                        );
                    }

                    //Atualizar
                    $sponsors_find = $this->model->findOrFail($form['id']);
                    //Apagar relacionamento
                    $sponsors_find->citiesSponsors()->detach();
                    foreach ($form['city'] as $city) {
                        //Cadastrar
                        $sponsors_find->citiesSponsors()->attach($city);

                    }
                    //Adiciona logs
                    $this->logs('patrocinadores->atualizar->' . $form['name'] . '|Cód.:' . $form['id']);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (Exception $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }
        } else {
            //Inserir novo
            try {
                //Verifica se houve upload de arquivo
                if (!isset($uploaded_file['banner']) and ($uploaded_file['banner']->file == "")) {
                    unset($form['filename']);
                }

                $created = $this->model->create($form);
                if ($created) {

                    //salvar o arquivo de banenr 1
                    if (isset($uploaded_file['banner']) and ($uploaded_file['banner']->file != "")) {
                        if ($file->getError() === UPLOAD_ERR_OK) {
                            $file->moveTo($filedirectory);
                            $image_resize = new \App\Helpers\Image\ImageResize($filedirectory);
                            $newnamefile = uniqid('pp_', true) . '.jpg';
                            $image_resize->save($directory . $newnamefile, IMAGETYPE_JPEG);
                            $form['filename'] = $newnamefile;
                        }
                    }


                    //Adicionar ao Local
                    $model_sponsors_local = new SponsorLocals();
                    foreach ($form['local'] as $local) {
                        $model_sponsors_local->create(
                            [
                                'sponsors_id' => $created->id,
                                'local' => $local
                            ]
                        );
                    }

                    //Adicionar a cidade
                    foreach ($form['city'] as $city) {
                        $sponsors = new Sponsors();
                        $sponsors_find = $sponsors->findOrFail($created->id);
                        $sponsors_find->citiesSponsors()->attach($city);
                    }

                    //Adiciona logs
                    $this->logs('patrocinadores->novo->' . $form['name'] . '|Cód:' . $created->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (\PDOException $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }

        }
    }

    public function delete($req, $res)
    {
        $id = $req->getAttribute('id');
        $entity = $this->model->find($id);

        try {
            //Apagar a imagem e depois remover o registro
            $directory = WWW_ROOT . 'img' . DS . 'sponsors' . DS;
    
            $entity->citiessponsors()->detach();
            $entity->localsponsors()->delete();

            if ($entity->delete()) {
                //Adiciona logs
                $this->logs('patrocinadores->apagar->' . $entity->title . '|Cod:' . $entity->id);
                unlink($directory . $entity->filename);
            }

            $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Registro removido com sucesso!']);
            return $res->withRedirect($this->redirect);
        } catch (RuntimeException $e) {
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
            return $res->withRedirect($this->redirect);
        }
    }


}
