<?php

namespace App\Controllers\Admin;

use App\Controllers\AppController;
use App\Models\Sponsors;
use App\Models\SponsorsClicksView;
use App\Helpers\PDFHeader;


class ReportsController extends AppController
{
    protected $pdf;

    public function __construct($container)
    {
        parent::__construct($container);

    }

    public function sponsors($req, $res)
    {

        $params = $req->getQueryParams();
        $sponsors = SponsorsClicksView::selectRaw('sum(id) as total, title, country, region, city')
            ->where('id',$params['id'])
            ->where('country', 'Brazil')
            ->whereBetween('created_at',['2019-03-01','2019-03-31'])
            ->groupBy('title', 'country', 'region', 'city')
            ->get();
        $entity = Sponsors::find($params['id']);
        $pdf= new PDFHeader();
        $pdf->AddPage();
        $pdf->Image(WWW_ROOT.DS.'img'.DS.'sponsors'.DS.$entity->filename,60,14,-350);
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(0, 0, 'Cliques (de 01/03/2019 a 31/03/2019)', 0, 0, 'C');
        $pdf->ln(10);
        $pdf->SetFont('Arial', '', 12);
        $pdf->Cell(0, 0, $entity->title, 0, 0, 'C');
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(50, 10, 'Estado', 0, 0, 'C');
        $pdf->Cell(50, 10, 'Cidade', 0, 0, 'C');
        $pdf->Cell(50, 10, 'Total', 0, 0, 'C');
        $pdf->ln(6);
        foreach ($sponsors as $item) {
            $pdf->SetFont('Arial', '', 10);
            $pdf->Cell(50, 10, $item->region!=""?$item->region:"---", 0, 0, 'C');
            $pdf->Cell(50, 10, $item->city!=""?$item->city:"---", 0, 0, 'C');
            $pdf->Cell(50, 10,$item->total, 0, 0, 'C');
            $pdf->ln(6);
        }
        $pdf->Cell(0, 5, '', 'B', 0, 'L');



//        //Redirecionar para o arquivo
        return $res->withStatus(200)
            ->withHeader('Content-Type', 'application/pdf')
            ->write($pdf->Output());

    }

};