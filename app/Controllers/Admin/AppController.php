<?php

namespace App\Controllers\Admin;

use App\App;
use App\Models\Logs;
use App\Models\Pages;
use App\Models\Users;

class AppController extends App
{

    public function __construct($container)
    {
        parent::__construct($container);
        $this->_user_name = $this->Session->getSession('cms_user_name') ? $this->Session->getSession('cms_user_name') : $this->Session->getCookie('cms_user_name');
        $this->_user_id = $this->Session->getSession('cms_user_id') ? $this->Session->getSession('cms_user_id') : $this->Session->getCookie('cms_user_id');
        $this->_user_permission = $this->Session->getSession('cms_user_permission') ? $this->Session->getSession('cms_user_permission') : $this->Session->getCookie('cms_user_permission');

        //Variavel Global do usuario
        $_user_model = new Users();
        $_user = $_user_model->find($this->_user_id);
        //Variáveis globais para o Twig
        $this->AdminView->getEnvironment()->addGlobal("_user", $_user);

    }

    public function logs($action, $id_user = "")
    {
        $model_logs = new Logs();
        $data = [
            'users_id' => (isset($id_user) and $id_user != "") ? $id_user : $this->Session->get('cms_user_id'),
            'action' => $action,
            'ip' => $this->get_client_ip()
        ];
        $model_logs->create($data);
    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
