<?php

namespace App\Controllers\Admin;

use App\Models\Cities;

class CitiesController extends AppController
{

    public $session_id_user;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->folder = 'template/cities';
        $this->AdminView->getEnvironment()->addGlobal("_page", 'Cidades');
        $this->AdminView->getEnvironment()->addGlobal("_page_single", 'Cidade');
        $this->redirect = $this->router->pathFor('admin.cities.index');
        $this->model = new Cities();
    }

    public function index($req, $res)
    {
        $collection = $this->model->get()->all();
        return $this->AdminView->render($res, $this->folder . '/index.twig', [
            'collection' => $collection
        ]);
    }

    public function form($req, $res)
    {
        //Pegar informação do banco e carregar o formulário
        $url = $req->getAttribute('url');

        if (isset($url) and $url != "") {
            $entity = $this->model->where('url', '=', $url)->get()->first();
        }
        $this->AdminView->render($res, $this->folder . '/form.twig', [
            'entity' => $entity
        ]);
    }

    public function save($req, $res)
    {
        //Formulário
        $form = $req->getParams();

        if ($form['id'] != "") {
            try {
                $form['url'] = $this->url_verify($form['url'],$this->model,$form['id']);

                $entity = $this->model->find($form['id']);
                if ($entity->update($form)) {
                    //Adiciona logs
                    $this->logs('cidade->atualizar->' . $form['title'] . '|Cód.:' . $form['id']);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (Exception $e) {
                echo $e;
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }
        } else {
            //Inserir novo
            try {
                $form['url'] = $this->url_verify($form['url'],$this->model,$form['id']);
                //Salva um novo registro
                $created = $this->model->create($form);

                if ($created) {
                    //Adiciona logs
                    $this->logs('cidade->novo->' . $form['title'] . '|Cód:' . $created->id);
                }

                $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Salvo com sucesso!']);
                return $res->withRedirect($this->redirect);
            } catch (\PDOException $e) {
                $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
                return $res->withRedirect($this->redirect);
            }

        }
    }

    public function delete($req, $res)
    {
        $id = $req->getAttribute('id');
        $entity = $this->model->find($id);

        try {
            $entity->delete();
            $this->flash->addMessage('msg', ['title' => 'Sucesso', 'type' => 'success', 'message' => 'Registro removido com sucesso!']);
            return $res->withRedirect($this->redirect);
        } catch (RuntimeException $e) {
            $this->flash->addMessage('msg', ['title' => 'Erro', 'type' => 'error', 'message' => 'Ocorreu um erro ao salvar!']);
            return $res->withRedirect($this->redirect);
        }
    }


}
