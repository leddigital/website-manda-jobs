<?php

namespace App\Controllers;

use App\Controllers\AppController;
use App\Models\Inscricao;
use App\Models\Candidato;
use PHPMailer\PHPMailer\PHPMailer;
use App\Models\Arquivos;
use Illuminate\Support\Facades\Session;
use App\Controllers\Vendor\Autoload;
use Exception;

class PagesController extends AppController
{
 
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function index($req, $res)
    {
        $post = $req->getParams();
        
        return $this->View->render(
           $res,
           'template/index.twig', ['emails' => $post] 
        );
    }

    public function recovery($req, $res){
        return $this->View->render(
            $res,
            'template/recovery.twig'
         );
    }

    public function auth($req, $res) {
        $get = $req->getQueryParams();

        return $this->View->render($res, 'template/login.twig',[
            'e'=>$get['e']
        ]);
    }

    public function salvar($req, $res) {

        $form = $req->getParams();

        if (array_key_exists("cpf_double_1", $form)) {
            
            $changeCPF = array();
            foreach ($form as $key => $value) {
                if($key === "cpf_double_1" || $key === "cpf_double_2"){
                    foreach ($value as $valor) {
                        $changeCPF[] = $valor;
                    }
                }
            }

            unset($form['cpf_double_1']);
            unset($form['cpf_double_2']);
            $form['cpf'] = $changeCPF;
     
        }

        $token_validacao = array();

        foreach($form['cpf'] as $k){
            array_push($token_validacao, md5(uniqid(time())));
        }
    
        $form["token_validacao"] = $token_validacao;

        foreach($form['cpf'] as $key1 => $fields){
            
            $form['cpf'][$key1] = str_replace(".", "", $form['cpf'][$key1]);
            $form['cpf'][$key1] = str_replace("-", "", $form['cpf'][$key1]);

            $form['data_nascimento'][$key1] = str_replace("/", "-", $form['data_nascimento'][$key1]);
            $form['data_nascimento'][$key1] = date("Y-m-d", strtotime($form['data_nascimento'][$key1]));

            foreach($form as $key2=>$c){
                $data[$key1][$key2] = $c[$key1];
            }
        }

        $candidato = new Candidato();

        foreach ($form['email'] as $email) {
            $entity = $candidato->where('email', '=', $email)->get()->first();
        }

        if($entity->email) {
            return $res->withRedirect($this->router->pathFor('index'));
        }

        $inscricao = new Inscricao();

        $ins_created = $inscricao->create();

        $created = $ins_created->candidatos()->createMany($data);

        $emails = array();
        
        foreach ($data as $key => $value) {
            
            $mail = new PHPMailer();
            
            $mail->CharSet = 'UTF-8';
            //$mail->SMTPDebug = 2;                                            
            $mail->isSMTP();                                                   
            $mail->Host = "mail.alfaagropecas.com.br";                      
            $mail->SMTPAuth = true;
            $mail->Username = "teste@alfaagropecas.com.br";
            $mail->Password = "teste@alfaagro";
            $mail->Port = 587;
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
            
            $mail->setFrom('teste@alfaagropecas.com.br', 'JSilva Paineis');
            $mail->addAddress($value["email"], 'Manda Jobs - Inscrição');
            $mail->addBCC('edinaldo@agencialed.com.br', 'Manda jobs - Cópia');
            $mail->isHTML(true);
            $mail->Subject = "Manda Jobs - complete sua inscrição!";
            $mail->MsgHTML($this->View->fetch('template/email/confirm.twig', ['token'=>$value["token_validacao"], 'nome'=>$value["nome"]]));
            $mail->send();

            $emails[] = $value["email"];
        }    

        if(!$created){
            echo "error";
        } else {
            return $res->withRedirect($this->router->pathFor('index', [], ['e' => $emails]));
        }
    }


    public function upload_comprovante($req, $res) {
        
        $id_inscricao = $this->Session->getSession('jsilva_id_inscricao');
        $nome = $this->Session->getSession('jsilva_nome_usuario');
        $sobrenome = $this->Session->getSession('jsilva_sobrenome_usuario');

        $form = $req->getParams();

        $id_inscricao = $this->Session->getSession('jsilva_id_inscricao');

        $uploaded_files = $req->getUploadedFiles();
       
        $file = $uploaded_files['arquivo'];
        $fileName = $file->getClientFilename();

        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $file_size = $file->getSize();

        if (($extension == 'pdf' || $extension == 'jpg' || $extension == "png") && $file_size <= 10000000) {
            $directory = WWW_ROOT . 'projects' . DS . $id_inscricao . DS;
            
            $time = date('d-m-Y H:i:s');

            $finalDirectory = $directory."COMPROVANTE ".$id_inscricao. " - " .$nome. " - " .$time. " - " .$fileName;
        
            mkdir($directory, 0777, true);
        
            if ($file->getError() === UPLOAD_ERR_OK) {
                $file->moveTo($finalDirectory);

                $inscricao = Inscricao::find($id_inscricao);

                $created = $inscricao->candidatos()->update(['comprovante_matricula' => $directory]);
                
                if($created) {
                    echo "ok";
                }
            }
            else {
                echo "error";
            }
        }
    }

    public function upload_project($req, $res) {

        $id_inscricao = $this->Session->getSession('jsilva_id_inscricao');
        $nome = $this->Session->getSession('jsilva_nome_usuario');
        $sobrenome = $this->Session->getSession('jsilva_sobrenome_usuario');
          
        $form = $req->getParams();

        $uploaded_files = $req->getUploadedFiles();

        $preview = $uploaded_files['preview'];
        $file = $uploaded_files['file'];

        $fileName = $file->getClientFilename();
        $previewFileName = $preview->getClientFilename();

        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $previewExtension = pathinfo($previewFileName, PATHINFO_EXTENSION);

        $file_size = $file->getSize();
        $preview_size = $preview->getSize();

        if (($extension == 'pdf' && $file_size <= 10000000) && 
            (($previewExtension == 'png' || $previewExtension == 'jpg' || $previewExtension == 'jpeg') && $preview_size <=  10000000)) {
            
            $entity = new Candidato();
            $row = $entity->where('id_inscricao', '=', $id_inscricao)->first();
            if(!isset($row)){
                echo "error";

            } else {

                $time = date('d-m-Y H:i:s');

                $directory = $row->comprovante_matricula;
                $finalDirectory = $directory."PROJETO ".$id_inscricao. " - " .$nome. " - " .$time. " - " .$fileName;
                $finalDirectoryPreview = $directory."PREVIEW ".$id_inscricao. " - " .$nome. " - " . $time. " - " .$previewFileName;               

                mkdir($directory, 0777, true);

                if ($file->getError() === UPLOAD_ERR_OK && $preview->getError() === UPLOAD_ERR_OK) {
                    
                    
                    try {
                        
                        $file->moveTo($finalDirectory);
                        $form['nome_arquivo_PDF'] = $finalDirectory;
                        $form['nome'] = $nome;
                        $form['sobrenome'] = $sobrenome;
                        $form['type'] = 'project';
                        $form['nome_arquivo'] = $fileName;

                        $inscricao = Inscricao::find($id_inscricao);
                        $inscricao->arquivos()->create($form);
                    
                        $preview->moveTo($finalDirectoryPreview);
                        $form['nome_arquivo_PDF'] = $finalDirectoryPreview;
                        $form['type'] = 'preview';
                        
                        $inscricao->arquivos()->create($form);
                        
                        echo "ok";
    
                    } catch (QueryException $e) {
                        dd($e);
                    }

                }
            }
        } else {
            echo "error";
        }
    }

    
    public function validate($req, $res){

        $token = $req->getParams();

        $candidato = new Candidato();
        $entity = $candidato->where("token_validacao", "=" , $token["token_validate"])->get()->first();

        if($token["token_validate"] == $entity->token_validacao && $entity->isValidated == false) {  
            return $this->View->render($res, 'insertpassword.twig', ['token' => $token['token_validate']]);
        } else {
            return $this->View->render($res, 'template/error.twig');
        }
    }

    public function validateRecover($req, $res){
        
        $tokenRecebido = $req->getAttribute('token');
        $candidato = new Candidato();
        $entity = $candidato->where('token_recuperacao', '=', $tokenRecebido)->get()->first();
        $tokenBanco = $entity->token_recuperacao;

        if ($tokenRecebido == $tokenBanco) {

            $this->Session->setSession('tokenRecover', $tokenBanco);
            $this->View->render($res, 'template/changepass.twig');

        } else {
            echo "<h4>TOKEN DE RECUPERAÇÃO INVÁLIDO.</h4>";
        }

    }

    public function changePass($req, $res){

        $post = $req->getParams();
        $pass = $post['pass'];
        $passcheck = $post['checkpass'];
        $token = $this->Session->getSession('tokenRecover');

        if ($pass == $passcheck) {

            $candidato = new Candidato();
            $entity = $candidato->where('token_recuperacao', '=', $token)->get()->first();
            $pass = password_hash($pass, PASSWORD_DEFAULT);
            $entity->update([ "pass" => $pass]);
            $entity->update(["token_recuperacao" => "unsigned"]);
            
            return $res->withRedirect($this->router->pathFor('auth',[],['ps'=>'true']));

        } else {
            echo "<h4>ERRO AO DEFINIR NOVA SENHA.</h4>";
        }
        
    }

    public function insertpassword($req, $res){
       
        $post = $req->getParams();
        $pass= $post["pass"][0];
        $passcheck = $post["passcheck"][0];
        $token = $post["token"][0];

        $candidato = new Candidato();
        $entity = $candidato->where("token_validacao", "=", $token)->get()->first();

        if ($pass === $passcheck) {

            $pass = password_hash($pass, PASSWORD_DEFAULT);

            $entity->update([ "pass" => $pass]);
            $entity->update(["isValidated" => true ]);
            $entity->update(["token_validacao" => "VALIDADO"]);
            
            return $res->withRedirect($this->router->pathFor('index'));

        } else {
            return $res->withRedirect($this->router->pathFor('error'));
        }
    }

    public function login ($req, $res) {

        $post = $req->getParams();

        $email_form = $post["user"][0];
        $senha_form = $post["password"][0];
        $keeplogged = $post['keeplogged'];

        $candidato = new Candidato();

        $entity = $candidato->where([['email', '=', $email_form]])->get()->first();

        $login_status = true;

        if ($entity->email === $email_form && password_verify($senha_form, $entity->pass)) {

             if ($entity->isValidated != true && $entity->token_validacao != "VALIDADO"){
               
                echo "VOCÊ AINDA NÃO VALIDOU SUA CONTA!";
                $login_status = false;

            } else {
                
                $this->Session->setSession('jsilva_id_inscricao', $entity->id_inscricao);
                $this->Session->setSession('jsilva_id_usuario', $entity->id);
                $this->Session->setSession('jsilva_nome_usuario', $entity->nome);
                $this->Session->setSession('jsilva_sobrenome_usuario', $entity->sobrenome);
                $this->Session->setSession('keeplogged', $keeplogged);

                $cookie = md5(uniqid(time()));
                $session = md5(uniqid(time()));
    
                $this->Session->setCookie('lgnCred', $cookie);
                $this->Session->setSession('SesCred', $session);
                
                return $res->withRedirect($this->router->pathFor('panel'));
            }

        } else {
            return $res->withRedirect($this->router->pathFor('auth',[],['e'=>'true']));
        }

        echo "<h1>".$login_status."</h1>";
    }

    public function recoverPassword($req, $res) {
        
        $post = $req->getParams();
        $formEmail = $post['email'];

        $candidato = new Candidato();
        $exEmail = $candidato->where('email', '=', $formEmail)->get()->first();

        if(isset($exEmail)){

            $email = $exEmail->email;
            $token_recuperacao = md5(uniqid((time())));
            $exEmail->update(['token_recuperacao' => $token_recuperacao]);

            $mail = new PHPMailer();

            $mail->CharSet = 'UTF-8';
            //$mail->SMTPDebug = 2;                                            
            $mail->isSMTP();                                                   
            $mail->Host = "mail.alfaagropecas.com.br";                      
            $mail->SMTPAuth = true;
            $mail->Username = "teste@alfaagropecas.com.br";
            $mail->Password = "teste@alfaagro";
            $mail->Port = 587;
            $mail->SMTPSecure = false;
            $mail->SMTPAutoTLS = false;
            
            $mail->setFrom('teste@alfaagropecas.com.br', 'JSilva Paineis');
            $mail->addAddress($email, 'Manda Jobs - Recuperação de senha');
            $mail->addBCC('edinaldo@agencialed.com.br', 'Manda jobs - Cópia');
            $mail->isHTML(true);
            $mail->Subject = "Manda Jobs - Recuperação de senha";
            $mail->MsgHTML($this->View->fetch('template/email/recover.twig', ['token_recuperacao' => $token_recuperacao]));
            $mail->send();

        } else {
            return $res->withRedirect($this->router->pathFor('recovery',[],['s'=>'true']));
        }

    }

    public function logout($req, $res){

        $this->Session->cookieUnset('lgnCred');
        return $res->withRedirect($this->router->pathFor('auth',[],['logout'=>'true']));
    }
    
    public function panel($req, $res) {

        $cookie = $this->Session->getCookie('lgnCred');

        if(!isset($cookie)){
           
            return $this->View->render($res, 'template/login.twig');

        } else {

            $keeplogged = $this->Session->getSession('keeplogged');
            if(!isset($keeplogged)){
                $this->Session->cookieUnset('lgnCred');
            }
            
            $id_inscricao = $this->Session->getSession('jsilva_id_inscricao');
            $id_usuario = $this->Session->getSession('jsilva_id_usuario');

            $arquivos = new Arquivos();
            $row = $arquivos->where("inscricao_id", "=", $id_inscricao)->orderBy("created_at", "DESC")->get();
            $first_entity = $row->first();
            $count_files = $row->count();
        
            $candidato = new Candidato();

            $entity = $candidato->where('id_inscricao', '=', $id_inscricao)->get()->all();
            $single_entity = $candidato->where([['id', '=', $id_usuario],['id_inscricao', '=', $id_inscricao]])->get()->first();

            if($first_entity->inscricao_id == $id_inscricao) {

                return $this->View->render($res, 'template/panel.twig', [
                    
                    'candidatos' => $entity, 
                    'single_candidato' => $single_entity, 
                    'files' => $count_files, 
                    'fileName' => $first_entity->nome_arquivo_PDF,
                    'rows' => $row

                    ]
                ); 

            } else {
                return $this->View->render($res, 'template/panel.twig', ['candidatos' => $entity, 'single_candidato' => $single_entity]);
            }
        } 
    }

    public function upload ($req, $res){

        $id_inscricao = $this->Session->getSession('jsilva_id_inscricao');
        $id_usuario = $this->Session->getSession('jsilva_id_usuario');

        $candidato = new Candidato();

        $entity = $candidato->where('id_inscricao', '=', $id_inscricao)->get()->all();
        $single_entity = $candidato->where([['id', '=', $id_usuario],['id_inscricao', '=', $id_inscricao]])->get()->first();

        return $this->View->render($res, 'template/upload.twig', ['candidatos' => $entity, 'single_candidato' => $single_entity]);
    }
    
    public function validate_cpf($req, $res) { 
        
        $post = $req->getParams();

        $candidato = new Candidato();

        $post['cpf'] = str_replace(".", "", $post['cpf']);
        $post['cpf'] = str_replace("-", "", $post['cpf']);
        
        $c = $candidato->where("cpf", "=", $post['cpf'])->get();
        
        
        echo "<pre>";
        print_r($c);
        echo "</pre>";

        // if($c->count()>0){
        //     echo true;
        // } else {
        //     echo false;
        // }
    }

    public function checkEmail($req, $res) {

        $post = $req->getParams();

        $formEmail = $post['email'][0];
        $typeRequest = $post['typeRequest'];
       
        $candidato = new Candidato();
        $exEmail = $candidato->where('email', '=', $formEmail)->get()->first();

        if ($typeRequest == 'recovery') {
            if(!isset($exEmail)){
                echo 'false';
            } 
        } else {
            if(isset($exEmail)){
                echo 'false';
            }
            else {
                echo 'true';
            }
        } 
    }

    public function checkCPF($req, $res) {

        $post = $req->getParams();
        $formCPF = $post['cpf'];

        $formCPF = trim($formCPF);
        $formCPF = str_replace("-", "", $formCPF);
        $formCPF = str_replace(".", "", $formCPF);

        $candidato = new Candidato();
        $exCPF = $candidato->where('cpf', '=', $formCPF)->get()->first();

        if(isset($exCPF)){
            echo 'false';
        } else {
            echo 'true';
        }
    }

};
