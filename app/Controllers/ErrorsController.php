<?php

namespace App\Controllers;

use App\Controllers\AppController;

class ErrorsController extends AppController
{
    public function __construct($container)
    {
        parent::__construct($container);
    }

    public function erro_404($req,$res)
    {
        return $this->View->render($res,'template/errors/404.twig');
    }

    public function erro_405()
    {
    }
}