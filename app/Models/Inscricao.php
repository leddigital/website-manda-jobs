<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscricao extends Model {

    protected $table = 'inscricao';
    protected $guarded = [];
    protected $fillable = [];
    public $timestamps = true;


    public function candidatos(){
        return $this->hasMany('App\Models\Candidato', 'id_inscricao');
    }

    public function inscricao(){
        return $this->belongsTo('App\Models\Inscricao');
    }

    public function arquivos(){
        return $this->hasMany('App\Models\Arquivos', 'inscricao_id', 'id');
    }

}

?>