<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidato extends Model
{

    protected $table = 'candidato';
    protected $guarded = [];
    protected $hidden = ['cidade'];
    protected $fillable = [];
    public $timestamps = true;

    public function inscricao(){
        return $this->belongsTo('App\Models\Inscricao','id');
    }

    public function requisicoes() {
        return $this->hasMany('App\Models\Requisicoes', 'idrequisicoes');
    }
}

?>