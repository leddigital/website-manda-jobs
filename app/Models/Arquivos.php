<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arquivos extends Model {

    protected $table = 'arquivos';
    protected $guarded = [];
    protected $fillable = [];
    public $timestamps = true;

    public function inscricao(){
        return $this->belongsTo('App\Models\Inscricao', 'inscricao_id', 'id');
    }

}

?>