<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logs extends Model {

    protected $table = 'inscricao';
    protected $guarded = [];
    protected $fillable = [];
    public $timestamps = true;


    public function users(){
        return $this->belongsTo('App\Models\Candidato');
    }

}

?>