<?php
define('DS', DIRECTORY_SEPARATOR);
define('DS_URL', '/');
define('ROOT', dirname(__DIR__));
define('APP_DIR', 'app');
define('APP', ROOT . DS . APP_DIR );
define('CONFIG', ROOT . DS . 'config'.DS);
define('WWW_ROOT', ROOT . DS . 'webroot' . DS);
