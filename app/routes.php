<?php

$app->get('[/]', 'App\Controllers\PagesController:index')->setName('index');
//$app->get('/#form[/]', 'App\Controllers\PagesController:index')->setName('form');
$app->get('/logout[/]', 'App\Controllers\PagesController:logout')->setName('logout');
$app->get('/teste[/]', 'App\Controllers\PagesController:teste')->setName('teste');
$app->get('/validate', 'App\Controllers\PagesController:validate')->setName('validate');
$app->get('/login[/]', 'App\Controllers\PagesController:auth')->setName('auth');
$app->get('/panel[/]','App\Controllers\PagesController:panel')->setName('panel');
$app->get('/recovery[/]','App\Controllers\PagesController:recovery')->setName('recovery');
$app->get('/validateRecover/{token}[/]','App\Controllers\PagesController:validateRecover')->setName('validateRecover');


$app->get('/upload[/]','App\Controllers\PagesController:upload')->setName('upload');


$app->post('/salvar[/]', 'App\Controllers\PagesController:salvar')->setName('salvar');
$app->post('/checkEmail[/]', 'App\Controllers\PagesController:checkEmail')->setName('checkEmail');
$app->post('/checkCPF[/]', 'App\Controllers\PagesController:checkCPF')->setName('checkCPF');
$app->post('/validate_cpf[/]', 'App\Controllers\PagesController:validate_cpf')->setName('validate_cpf');
$app->post('/insertpassword[/]', 'App\Controllers\PagesController:insertpassword')->setName('insertpassword');
$app->post('/auth[/]', 'App\Controllers\PagesController:login')->setName('login');
$app->post('/upload_project[/]', 'App\Controllers\PagesController:upload_project')->setName('upload_project');
$app->post('/upload_comprovante[/]', 'App\Controllers\PagesController:upload_comprovante')->setName('upload_comprovante');
$app->post('/recoverPassword[/]','App\Controllers\PagesController:recoverPassword')->setName('recoverPassword');
$app->post('/changePass[/]','App\Controllers\PagesController:changePass')->setName('changePass');



/*
 * Rotas de Erro
 * */
$app->get('/404[/]', 'App\Controllers\ErrorsController:erro_404')->setName('erro404');
$app->get('/405[/]', 'App\Controllers\ErrorsController:erro_404')->setName('erro405');


/*
 * ROTAS DO ADMIN
 * */
$app->group("/admin", function () {

    $this->get("[/]", 'App\Controllers\Admin\PostsController:index')->setName('admin.news.index.dashboard');

    //Login/Logout de Usuários
    $this->get("/login[/]", 'App\Controllers\Admin\UsersController:login')->setName('admin.login');
    $this->post("/user/login[/]", 'App\Controllers\Admin\UsersController:user_login')->setName('admin.users.login');
    $this->get("/user/logout[/]", 'App\Controllers\Admin\UsersController:user_logout')->setName('admin.users.logout');

    $this->get("/usuario/esqueci-minha-senha[/]", 'App\Controllers\Admin\UsersController:forgot_password')->setName('admin.users.forgot');
    $this->post("/usuario/esqueci-minha-senha/enviar[/]", 'App\Controllers\Admin\UsersController:reset_password')->setName('admin.user.reset.password');

    //Reports

    $this->get("/relatorio[/]", 'App\Controllers\Admin\ReportsController:sponsors')->setName('admin.reports.sponsors');


    //Logs
    $this->get("/logs[/]", 'App\Controllers\Admin\LogsController:index')->setName('admin.logs.index');

    $this->get("/image[/]", 'App\Controllers\Admin\PostsController:image')->setName('admin.image');

    //Usuarios
    $this->get("/usuarios[/]", 'App\Controllers\Admin\UsersController:index')->setName('admin.users.index');
    $this->get("/usuarios/cadastro[/]", 'App\Controllers\Admin\UsersController:form')->setName('admin.users.form');
    $this->post("/usuarios/cadastro/salvar[/]", 'App\Controllers\Admin\UsersController:save')->setName('admin.users.save');
    $this->get("/usuarios/cadastro/apagar/{id}[/]", 'App\Controllers\Admin\UsersController:delete')->setName('admin.users.delete');
    $this->get("/usuarios/cadastro/{id}[/]", 'App\Controllers\Admin\UsersController:form')->setName('admin.users.form.edit');

    //Cidades
    $this->get("/cidades[/]", 'App\Controllers\Admin\CitiesController:index')->setName('admin.cities.index');
    $this->get("/cidades/cadastro[/]", 'App\Controllers\Admin\CitiesController:form')->setName('admin.cities.form');
    $this->post("/cidades/cadastro/salvar[/]", 'App\Controllers\Admin\CitiesController:save')->setName('admin.cities.save');
    $this->get("/cidades/cadastro/apagar/{id}[/]", 'App\Controllers\Admin\CitiesController:delete')->setName('admin.cities.delete');
    $this->get("/cidades/cadastro/{url}[/]", 'App\Controllers\Admin\CitiesController:form')->setName('admin.cities.form.edit');

    //Categorias
    $this->get("/categorias[/]", 'App\Controllers\Admin\CategoriesController:index')->setName('admin.categories.index');
    $this->get("/categorias/cadastro[/]", 'App\Controllers\Admin\CategoriesController:form')->setName('admin.categories.form');
    $this->post("/categorias/cadastro/salvar[/]", 'App\Controllers\Admin\CategoriesController:save')->setName('admin.categories.save');
    $this->get("/categorias/cadastro/apagar/{id}[/]", 'App\Controllers\Admin\CategoriesController:delete')->setName('admin.categories.delete');
    $this->get("/categorias/cadastro/{url}[/]", 'App\Controllers\Admin\CategoriesController:form')->setName('admin.categories.form.edit');


    //Sponsors
    $this->get("/patrocinadores[/]", 'App\Controllers\Admin\SponsorsController:index')->setName('admin.sponsors.index');
    $this->get("/patrocinadores/cadastro[/]", 'App\Controllers\Admin\SponsorsController:form')->setName('admin.sponsors.form');
    $this->post("/patrocinadores/cadastro/salvar[/]", 'App\Controllers\Admin\SponsorsController:save')->setName('admin.sponsors.save');
    $this->get("/patrocinadores/cadastro/apagar/{id}[/]", 'App\Controllers\Admin\SponsorsController:delete')->setName('admin.sponsors.delete');
    $this->get("/patrocinadores/cadastro/{id}[/]", 'App\Controllers\Admin\SponsorsController:form')->setName('admin.sponsors.form.edit');


    //Páginas
    $this->get("/noticias[/]", 'App\Controllers\Admin\PostsController:index')->setName('admin.posts.index');
    $this->get("/noticias/cadastro[/]", 'App\Controllers\Admin\PostsController:form')->setName('admin.posts.form');
    $this->post("/noticias/cadastro/salvar[/]", 'App\Controllers\Admin\PostsController:save')->setName('admin.posts.save');
    $this->get("/noticias/cadastro/apagar/{id}[/]", 'App\Controllers\Admin\PostsController:delete')->setName('admin.posts.delete');
    $this->get("/noticias/cadastro/{url}[/]", 'App\Controllers\Admin\PostsController:form')->setName('admin.posts.form.edit');

    $this->get("/ajax/noticias[/]", 'App\Controllers\Admin\PostsController:paginate')->setName('admin.ajax.posts');
})->add(new App\Helpers\CheckSessionCms($container));
