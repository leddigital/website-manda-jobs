<?php
/**
 * Created by PhpStorm.
 * User: Enzo
 * Date: 16/01/2019
 * Time: 22:33
 */

namespace App\Helpers;

class TwigHtmlHelpers extends \Twig_Extension
{
    /**
     * @var \Slim\Interfaces\RouterInterface
     */
    private $router;
    
    /**
     * @var string|\Slim\Http\Uri
     */
    private $uri;
    
    private $container;
    
    public function __construct($router, $uri, $container)
    {
        $this->router = $router;
        $this->uri = $uri;
        $this->container = $container;
    }
    
    public function getName()
    {
        return 'slim';
    }
    
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('baseImg', array($this, 'baseImg')),
            new \Twig_SimpleFunction('baseJs', array($this, 'baseJs')),
            new \Twig_SimpleFunction('baseCss', array($this, 'baseCss')),
            new \Twig_SimpleFunction('basePlugins', array($this, 'basePlugins')),
        ];
    }
    
    public function baseImg($admin = false)
    {
        if ($admin == true) {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['adminPath'] . $this->container['settings']['imgPath'];
            } else {
                return $this->container['settings']['adminPath'] . $this->container['settings']['imgPath'];
            }
        } else {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['imgPath'];
            } else {
                return $this->container['settings']['imgPath'];
            }
        }
    }
    
    public function baseJs($admin = false)
    {
        if ($admin == true) {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['adminPath'] . $this->container['settings']['scriptPath'];
            } else {
                return $this->container['settings']['adminPath'] . $this->container['settings']['scriptPath'];
            }
        } else {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['scriptPath'];
            } else {
                return $this->container['settings']['scriptPath'];
            }
        }
    }
    
    public function baseCss($admin = false)
    {
        if ($admin == true) {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['adminPath'] . $this->container['settings']['cssPath'];
            } else {
                return $this->container['settings']['adminPath'] . $this->container['settings']['cssPath'];
            }
        } else {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['cssPath'];
            } else {
                return $this->container['settings']['cssPath'];
            }
        }
    }
    
    public function basePlugins($admin = false)
    {
        if ($admin == true) {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['adminPath'] . $this->container['settings']['pluginsPath'];
            } else {
                return $this->container['settings']['adminPath'] . $this->container['settings']['pluginsPath'];
            }
        } else {
            if ($this->container['settings']['fullPathUrl'] == true) {
                return $this->uri->getBaseUrl() . $this->container['settings']['pluginsPath'];
            } else {
                return $this->container['settings']['pluginsPath'];
            }
        }
    }
    
    public function baseUrl()
    {
        if (is_string($this->uri)) {
            return $this->uri;
        }
        if (method_exists($this->uri, 'getBaseUrl')) {
            return $this->uri->getBaseUrl();
        }
    }
    
    
    
//
//    public function isCurrentPath($name, $data = [])
//    {
//        return $this->router->pathFor($name, $data) === $this->uri->getBasePath() . '/' . ltrim($this->uri->getPath(), '/');
//    }
//
//    /**
//     * Returns current path on given URI.
//     *
//     * @param bool $withQueryString
//     * @return string
//     */
//    public function currentPath($withQueryString = false)
//    {
//        if (is_string($this->uri)) {
//            return $this->uri;
//        }
//
//        $path = $this->uri->getBasePath() . '/' . ltrim($this->uri->getPath(), '/');
//
//        if ($withQueryString && '' !== $query = $this->uri->getQuery()) {
//            $path .= '?' . $query;
//        }
//
//        return $path;
//    }
//
//    /**
//     * Set the base url
//     *
//     * @param string|Slim\Http\Uri $baseUrl
//     * @return void
//     */
//    public function setBaseUrl($baseUrl)
//    {
//        $this->uri = $baseUrl;
//    }
}
