<?php

namespace App\Helpers;


use App\Helpers\FPDF\tFPDF;

class PDFHeader extends \tFPDF
{
    public function __construct($orientation = 'P', $unit = 'mm', $size = 'A4')
    {
        parent::__construct($orientation, $unit, $size);
    }


    //HEADER
    public function Header()
    {
        $this->Image(WWW_ROOT.DS.'img'.DS.'logo.png',10,14,-350);
        $this->Ln(6);
        $this->SetFont('Arial', 'BI', 16);
        $this->Cell(25, 0, '', 0, 0, 'C');
        $this->Cell(0, 4,'', 0, 0, 'C');
        $this->Ln(4);
        $this->SetFont('Arial', '', 11);
        $this->Cell(25, 0, '', 0, 0, 'C');
        $this->Cell(0, 8,'', 0, 0, 'C');
        $this->Ln(15);
        $this->Cell(0, 5, '', 'B', 0, 'L');
        $this->Ln(15);
    }
    //FOOTER
    public function Footer()
    {

        $this->SetY(-15);
        $this->SetFont('Arial', 'I', 8);
        $this->Cell(0, 0, '', 1, 0, 'C');
        $this->Ln(2);
        $this->Cell(0, 10, 'Página' . $this->PageNo(), 0, 0, 'C');
    }
}