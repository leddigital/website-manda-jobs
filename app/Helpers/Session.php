<?php

namespace App\Helpers;


class Session
{
    public function __construct()
    {
        $this->session_start();
    }


    public function session_start(){
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

//    public function session_destroy(){
//        session_destroy();
//    }

    public function setSession($name,$value){
        $_SESSION[$name] = $value;
    }

    public function setCookie($name,$value){
        setcookie($name, $value, time()+3600*24*30, '/');
    }

    public function get($name){
        return isset($_SESSION[$name])?$_SESSION[$name]:$_COOKIE[$name];
    }

    public function getSession($name){
        return $_SESSION[$name];
    }

    public function getCookie($name){
        return $_COOKIE[$name];
    }

    public function sessionUnset($name){
        unset($_SESSION[$name]);
    }

    public function cookieUnset($name){
        setcookie($name, null, -1, '/');
    }
}