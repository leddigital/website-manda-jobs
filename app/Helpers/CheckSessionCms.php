<?php

namespace App\Helpers;

use App\App;


class CheckSessionCms extends App {

    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function __invoke($req, $res, $next)
    {
        //Validar usuario e senha
        $sessions = $this->container->Session;
        $public_paths = $this->container['settings']['public_paths'];
        $uri = $req->getUri()->getPath();
        $method = $req->getMethod();

        //Verifica se a sessão é valida
        if($sessions->getSession('cms_user_id')!="" or $sessions->getCookie('cms_user_id')!=""){
            return $next($req,$res);
        }else{
            if($this->in_array_key($public_paths,$uri,$method)){
                return $next($req, $res);
            }else {
                return $res->withRedirect($this->container->router->pathFor('admin.login'));
            }

        }
       return $res;
    }
}
