# Instruções de Uso
## Básico
#### Instalação
Após baixa-lo do Git, execute o comando **composer install**.
1. Crie o banco de dados
2. Edite o arquivo *settings.php* que se encontra na pasta **./app**

```php
...
  'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'database' => 'nome_banco',
        'username' => 'usuario_banco',
        'password' => '**********',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => '',
    ],
...
```

#### Estrutura das Pastas
Pasta                   | Descrição
---------               | ------
./app                   | Arquivos do MVC, possui também os arquivos de rotas, middleware e configurações
./app/Controllers       | Controllers
./app/Helpers           | Helpers
./app/Models            | Models
./app/View              | View
./webroot               | Nessa pasta é onde se encontra os arquivos estáticos (css, javascript e imagens), nao modificar as pastas, caso mude-as, alterar as configurções em *settings.php*.

**settings.php**
```php
...
//settings.php -> alterar as pastas
'imgPath' => '/img/',
'scriptPath' => '/js/',
'cssPath' => '/css/',
'pluginsPath' => '/plugins/',
...
```

## Twig 
##### https://twig.symfony.com/doc/2.x/
#### Algumas considerações de arquivos estáticos
Código                  | Descrição
---------               | ------
{{ base_url() }}        | Define a URL base dentro do Twig <br> Ex.: **```<a href='{{ base_url() }}contato'>Link</a>```**
{{ baseCss() }}         | Base da pasta de CSS, definida em **settings.php** <br> Ex.: **```<link href='{{ baseCss() }}style.css' rel="stylesheet" type="text/css" />```**
{{ baseJs() }}          | Base da pasta de JavaScripts, definida em **settings.php** <br> Ex.: **```<script src="{{ baseJs }}global.js"></script>```**
{{ baseImg() }}         | Base da pasta de Imagens, definida em **settings.php** <br> Ex.: **```<img src="{{ baseImg() }}image.jpg" />```**